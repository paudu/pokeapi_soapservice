package com.test.pokeapi.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "Informacion_request")
public class InformacionRequest {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id_request;
    @Column(nullable = false)
    private String ip_origen;
    @Column(nullable = false)
    private Date fecha_request;
    @Column(nullable = false)
    private String metodo_request;

    public Integer getId_request() {
        return id_request;
    }

    public void setId_request(Integer id_request) {
        this.id_request = id_request;
    }

    public String getIp_origen() {
        return ip_origen;
    }

    public void setIp_origen(String ip_origen) {
        this.ip_origen = ip_origen;
    }

    public Date getFecha_request() {
        return fecha_request;
    }

    public void setFecha_request(Date fecha_request) {
        this.fecha_request = fecha_request;
    }

    public String getMetodo_request() {
        return metodo_request;
    }

    public void setMetodo_request(String metodo_request) {
        this.metodo_request = metodo_request;
    }
}
