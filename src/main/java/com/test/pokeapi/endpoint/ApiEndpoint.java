package com.test.pokeapi.endpoint;

import com.example.soap_web_service.*;
import com.test.pokeapi.entity.InformacionRequest;
import com.test.pokeapi.service.InformacionRequestService;
import jakarta.servlet.http.HttpServletRequest;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import java.io.IOException;
import java.sql.Date;
import java.util.Calendar;

@Endpoint
@Component
public class ApiEndpoint {
        private final OkHttpClient httpClient = new OkHttpClient();
        private static final String NAMESPACE_URI = "http://example.com/soap-web-service";

        private HttpServletRequest httpServletRequest;

        @Autowired
        private InformacionRequestService informacionRequestService;

        @Autowired
        public void setRequest(HttpServletRequest request) {
            this.httpServletRequest = request;
        }

        /*
        Metodo que regresa las habilidades de un pokemon.
         */
        @PayloadRoot(namespace = NAMESPACE_URI, localPart = "AbilitiesRequest")
        @ResponsePayload
        public AbilitiesResponse getAbilities(@RequestPayload AbilitiesRequest request) throws IOException {
            AbilitiesResponse abilitiesResponse = new AbilitiesResponse();
            if (request.getName()!=null && !request.getName().isEmpty()) {
                Request requestPokeApi = new Request.Builder()
                        .url("http://pokeapi.co/api/v2/pokemon/")
                        .build();

                try (Response response = httpClient.newCall(requestPokeApi).execute()) {
                    JSONObject result = new JSONObject(response.body().string());
                   JSONArray jsonArray=(JSONArray) result.get("results");
                   for(int x=0; x<jsonArray.length();x++){
                       JSONObject jsonObject = jsonArray.getJSONObject(x);
                       if(jsonObject.get("name").toString().equals(request.getName())){
                           abilitiesResponse.setAbilities(extractInfoAbilities(jsonObject.get("url").toString()));
                       }
                   }

                }catch (IOException e) {
                    throw new RuntimeException(e);
                }
                saveInfoRequest("getAbilities");
            }
                return abilitiesResponse;

        }

        /*
        Metodo auxuliar de getAbilities
         */
        public String extractInfoAbilities(String url){
            String abilities=" ";
            Request requestPokeApi = new Request.Builder()
                    .url(url)
                    .build();

            try (Response response = httpClient.newCall(requestPokeApi).execute()) {
                JSONObject result = new JSONObject(response.body().string());
                JSONArray jsonArray=(JSONArray) result.get("abilities");
                if(!jsonArray.isEmpty()) {
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject ability = (JSONObject) jsonArray.get(i);
                        abilities = abilities + "," + (ability.getJSONObject("ability").get("name").toString());
                    }
                }
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            return abilities;
        }

        /*
        Metodo que regresa la base experience de un pokemon
         */
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "BaseExperienceRequest")
    @ResponsePayload
    public BaseExperienceResponse getBaseExperience(@RequestPayload BaseExperienceRequest request){
        BaseExperienceResponse baseExperienceResponse= new BaseExperienceResponse() ;
        if (request.getName()!=null && !request.getName().isEmpty()) {
            Request requestPokeApi = new Request.Builder()
                    .url("http://pokeapi.co/api/v2/pokemon/")
                    .build();

            try (Response response = httpClient.newCall(requestPokeApi).execute()) {
                JSONObject result = new JSONObject(response.body().string());
                JSONArray jsonArray=(JSONArray) result.get("results");
                for(int x=0; x<jsonArray.length();x++){
                    JSONObject jsonObject = jsonArray.getJSONObject(x);
                    if(jsonObject.get("name").toString().equals(request.getName())){
                        baseExperienceResponse.setBaseExperience(extractInfoBaseExp(jsonObject.get("url").toString()));
                    }
                }

            }catch (IOException e) {
                throw new RuntimeException(e);
            }
            saveInfoRequest("getBaseExperience");
        }

        return baseExperienceResponse;
    }

    /*
        Metodo auxiliar de getBaseExperience
     */
    public String extractInfoBaseExp(String url){
        String baseExperience="";
        Request requestPokeApi = new Request.Builder()
                .url(url)
                .build();

        try (Response response = httpClient.newCall(requestPokeApi).execute()) {
            JSONObject result = new JSONObject(response.body().string());
            baseExperience= result.get("base_experience").toString();

        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        return baseExperience;
    }

    /*
        Metodo que regresa los held items de un pokemon
         */
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "HeldItemsRequest")
    @ResponsePayload
    public HeldItemsResponse getHeldItems(@RequestPayload HeldItemsRequest request){
        HeldItemsResponse heldItemsResponse= new HeldItemsResponse();
        if (request.getName()!=null && !request.getName().isEmpty()) {
            Request requestPokeApi = new Request.Builder()
                    .url("http://pokeapi.co/api/v2/pokemon/")
                    .build();

            try (Response response = httpClient.newCall(requestPokeApi).execute()) {
                JSONObject result = new JSONObject(response.body().string());
                JSONArray jsonArray=(JSONArray) result.get("results");
                for(int x=0; x<jsonArray.length();x++){
                    JSONObject jsonObject = jsonArray.getJSONObject(x);
                    if(jsonObject.get("name").toString().equals(request.getName())){
                       heldItemsResponse.setHeldItems(extractInfoHeldItems(jsonObject.get("url").toString()));
                    }
                }

            }catch (IOException e) {
                throw new RuntimeException(e);
            }
            saveInfoRequest("getHeldItems");
        }

        return heldItemsResponse;
    }

    /*
    Metodo auxiliar de getHeldItems
   */
    public String extractInfoHeldItems(String url){
        String heldItems=" ";
        Request requestPokeApi = new Request.Builder()
                .url(url)
                .build();

        try (Response response = httpClient.newCall(requestPokeApi).execute()) {
            JSONObject result = new JSONObject(response.body().string());
            JSONArray jsonArray=(JSONArray) result.get("held_items");
            if(!jsonArray.isEmpty()) {
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject ability = (JSONObject) jsonArray.get(i);
                    heldItems = heldItems + "," + (ability.getJSONObject("item").get("name").toString());
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return heldItems;
    }

    /*
        Metodo que regresa el id de un pokemon
         */
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "IdRequest")
    @ResponsePayload
    public IdResponse getId(@RequestPayload IdRequest request){
        IdResponse idResponse= new IdResponse();
        if (request.getName()!=null && !request.getName().isEmpty()) {
            Request requestPokeApi = new Request.Builder()
                    .url("http://pokeapi.co/api/v2/pokemon/")
                    .build();

            try (Response response = httpClient.newCall(requestPokeApi).execute()) {
                JSONObject result = new JSONObject(response.body().string());
                JSONArray jsonArray=(JSONArray) result.get("results");
                for(int x=0; x<jsonArray.length();x++){
                    JSONObject jsonObject = jsonArray.getJSONObject(x);
                    if(jsonObject.get("name").toString().equals(request.getName())){
                        idResponse.setId(extractInfoId(jsonObject.get("url").toString()));
                    }
                }

            }catch (IOException e) {
                throw new RuntimeException(e);
            }
            saveInfoRequest("getId");
        }

        return idResponse;
    }

    /*
    Metodo auxiliar de getId
   */
    public String extractInfoId(String url){
        String id="";
        Request requestPokeApi = new Request.Builder()
                .url(url)
                .build();

        try (Response response = httpClient.newCall(requestPokeApi).execute()) {
            JSONObject result = new JSONObject(response.body().string());
            id=result.get("id").toString();

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return id;
    }

    /*
       Metodo que regresa el location_are_encounters  de un pokemon
        */
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "LocationRequest")
    @ResponsePayload
    public LocationResponse getLocation(@RequestPayload LocationRequest request){
        LocationResponse locationResponse= new LocationResponse();
        if (request.getName()!=null && !request.getName().isEmpty()) {
            Request requestPokeApi = new Request.Builder()
                    .url("http://pokeapi.co/api/v2/pokemon/")
                    .build();

            try (Response response = httpClient.newCall(requestPokeApi).execute()) {
                JSONObject result = new JSONObject(response.body().string());
                JSONArray jsonArray=(JSONArray) result.get("results");
                for(int x=0; x<jsonArray.length();x++){
                    JSONObject jsonObject = jsonArray.getJSONObject(x);
                    if(jsonObject.get("name").toString().equals(request.getName())){
                        locationResponse.setLocationAreaEncounters(extractInfoLocation(jsonObject.get("url").toString()));
                    }
                }

            }catch (IOException e) {
                throw new RuntimeException(e);
            }
            saveInfoRequest("getLocation");
        }

        return locationResponse;
    }

    /*
    Metodo auxiliar de getLocation
  */
    public String extractInfoLocation(String url){
        String urlLocation="";
        String locations= "";
        Request requestPokeApi = new Request.Builder()
                .url(url)
                .build();

        try (Response response = httpClient.newCall(requestPokeApi).execute()) {
            JSONObject result = new JSONObject(response.body().string());
            urlLocation=result.get("location_area_encounters").toString();

        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        Request requestLocation = new Request.Builder()
                .url(urlLocation)
                .build();

        try (Response response = httpClient.newCall(requestLocation).execute()) {
            JSONArray jsonArray= new JSONArray(response.body().string());
            if(!jsonArray.isEmpty()) {
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject ability = (JSONObject) jsonArray.get(i);
                    locations = locations + "," + (ability.getJSONObject("location_area").get("name").toString());
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        return locations;
    }

    /*
     Metodo que regresa el nombre de un pokemon
      */
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "NameRequest")
    @ResponsePayload
    public NameResponse getName(@RequestPayload NameRequest request){
        NameResponse nameResponse= new NameResponse();
        if (request.getName()!=null && !request.getName().isEmpty()) {
            Request requestPokeApi = new Request.Builder()
                    .url("http://pokeapi.co/api/v2/pokemon/")
                    .build();

            try (Response response = httpClient.newCall(requestPokeApi).execute()) {
                JSONObject result = new JSONObject(response.body().string());
                JSONArray jsonArray=(JSONArray) result.get("results");
                for(int x=0; x<jsonArray.length();x++){
                    JSONObject jsonObject = jsonArray.getJSONObject(x);
                    if(jsonObject.get("name").toString().equals(request.getName())){
                        nameResponse.setName(extractInfoNName(jsonObject.get("url").toString()));
                    }
                }

            }catch (IOException e) {
                throw new RuntimeException(e);
            }
            saveInfoRequest("getName");
        }

        return nameResponse;
    }

    /*
   Metodo auxiliar de getName
  */
    public String extractInfoNName(String url){
        String name="";
        Request requestPokeApi = new Request.Builder()
                .url(url)
                .build();

        try (Response response = httpClient.newCall(requestPokeApi).execute()) {
            JSONObject result = new JSONObject(response.body().string());
            name=result.get("name").toString();

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return name;
    }

    /*
    Metodo que guarda la informacion de las peticiones en la bd.
     */
    public void saveInfoRequest(String metodo){
        InformacionRequest informacionRequest= new InformacionRequest();
        informacionRequest.setIp_origen(this.httpServletRequest.getRemoteAddr());
        Calendar calendar= Calendar.getInstance();
        informacionRequest.setFecha_request(new Date(calendar.getTimeInMillis()));
        informacionRequest.setMetodo_request(metodo);
        informacionRequestService.saveInfo(informacionRequest);
    }


    }
