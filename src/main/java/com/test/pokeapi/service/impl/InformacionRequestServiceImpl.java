package com.test.pokeapi.service.impl;

import com.test.pokeapi.entity.InformacionRequest;
import com.test.pokeapi.repository.InformacionRequestRepository;
import com.test.pokeapi.service.InformacionRequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class InformacionRequestServiceImpl implements InformacionRequestService {

    @Autowired
    private InformacionRequestRepository informacionRequestRepository;

    @Override
    public InformacionRequest saveInfo(InformacionRequest informacionRequest) {
        return informacionRequestRepository.save(informacionRequest);
    }
}
