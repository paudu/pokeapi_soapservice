package com.test.pokeapi.service;

import com.test.pokeapi.entity.InformacionRequest;

public interface InformacionRequestService {

    InformacionRequest saveInfo(InformacionRequest informacionRequest);
}
