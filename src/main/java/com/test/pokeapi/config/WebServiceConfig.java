package com.test.pokeapi.config;

import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.ws.config.annotation.EnableWs;
import org.springframework.ws.config.annotation.WsConfigurerAdapter;
import org.springframework.ws.transport.http.MessageDispatcherServlet;
import org.springframework.ws.wsdl.wsdl11.DefaultWsdl11Definition;
import org.springframework.ws.wsdl.wsdl11.Wsdl11Definition;
import org.springframework.xml.xsd.SimpleXsdSchema;

@EnableWs
@Configuration
public class WebServiceConfig extends WsConfigurerAdapter {

    @Bean
    public ServletRegistrationBean messageDispatcherServlet(ApplicationContext context) {
        MessageDispatcherServlet servlet = new MessageDispatcherServlet();
        servlet.setApplicationContext(context);
        servlet.setTransformWsdlLocations(true);
        return new ServletRegistrationBean<>(servlet, "/soap-pokeapi/*");
    }

    @Bean(name = "pokeApi")
    public Wsdl11Definition pokeApiWsdl11Definition() {
        DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
        wsdl11Definition.setPortTypeName("PokeApiPortType");
        wsdl11Definition.setLocationUri("/soap-pokeapi");
        wsdl11Definition.setTargetNamespace("http://example.com/soap-web-service");
        wsdl11Definition.setSchema(pokeApiSchema());
        return wsdl11Definition;
    }
    @Bean
    public SimpleXsdSchema pokeApiSchema() {
        return new SimpleXsdSchema(new ClassPathResource("pokeApi.xsd"));
    }
}
