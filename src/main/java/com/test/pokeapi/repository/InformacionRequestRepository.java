package com.test.pokeapi.repository;

import com.test.pokeapi.entity.InformacionRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InformacionRequestRepository extends JpaRepository<InformacionRequest, Integer> {
}
