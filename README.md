# PokeApi_SoapService

PokeAip_SoapService es un proyecto donde se expone un servicio soap, a partir del consumo de un Api rest 
llamada PokeApi https://pokeapi.co/ , los metodos del servicio soap se explican a continuacion:

### Instalacion

- 1 Clonar proyecto.
 ```bash
    git clone https://gitlab.com/paudu/pokeapi_soapservice.git
 ```
- 2 Abrir proyecto.

 Una vez que tenga el proyecto en el destino donde eligas guardalo, abrelo con tu IDE
de preferencia. 

- Abrir proyecto con Intellij -> https://www.jetbrains.com/guide/java/tutorials/import-project/open-project/
- Abrir proyecto con Eclipse -> https://eclipse.dev/escet/use/projects.html

Ahora podras correr el proyecto (Run), una vez que el proyecto este corriendo, la siguiente url 
te mostrara el wsdl del servicio soap, en donde se describen los diferentes metodos que contiene.

 ```bash
    http://localhost:8080/soap-pokeapi/pokeApi.wsdl
 ```


## Base de Datos

Dentro del proyecto se utilizan properties para la conexion a la base de datos,
para probar el proyecto es necesario que estos sean ajustado, a continuacion se muestran estas properties y los datos que deben ser ajustado.


 ```properties
    spring.datasource.url= jdbc:postgresql://localhost:5432/pokeapi
    spring.datasource.username= [username]
    spring.datasource.password= [password]
    spring.jpa.hibernate.ddl-auto = update
    spring.jpa.properties.hibernate.dialect=org.hibernate.dialect.PostgreSQLDialect
 ```

Para crear la Base de datos, es necesario ejecutar el siguiente script:

```sql
-- Database: pokeapi

-- DROP DATABASE IF EXISTS pokeapi;

CREATE DATABASE pokeapi
WITH
OWNER = [username]
ENCODING = 'UTF8'
LC_COLLATE = 'C'
LC_CTYPE = 'C'
TABLESPACE = pg_default
CONNECTION LIMIT = -1
IS_TEMPLATE = False;
 ```

Para crear la tabla, es necesario ejecutar el siguiente script:

```sql
-- Table: public.Informacion_request

-- DROP TABLE IF EXISTS public."Informacion_request";

CREATE TABLE IF NOT EXISTS public."Informacion_request"
(
    ip_origen character varying(20) COLLATE pg_catalog."default",
    id_request integer NOT NULL DEFAULT nextval('"Informacion_request_id_request_seq"'::regclass),
    fecha_request date,
    metodo_request character varying(20) COLLATE pg_catalog."default",
    CONSTRAINT "Informacion_request_pkey" PRIMARY KEY (id_request)
)
 ```

### Test

Para probar los diferentes metodos del servicio soap puedes utilizar SoapUI 
- Instalacion: https://www.soapui.org/downloads/soapui/
- Probar Servicio Soap: https://www.soapui.org/getting-started/soap-test/
 

### Metodos Soap Service 

 El servicio soap que en el proyecto se expone, tiene 6 metodos, cada uno regresa informacion de un pokemon dado el request mandado.

- getAbilities - Este metodo retorna las habilidades de un pokemon.

Request
```xml
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:soap="http://example.com/soap-web-service">
    <soapenv:Header/>
    <soapenv:Body>
        <soap:AbilitiesRequest>
            <soap:name>bulbasaur</soap:name>
        </soap:AbilitiesRequest>
    </soapenv:Body>
</soapenv:Envelope>
```

Response 
```xml
<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">
    <SOAP-ENV:Header/>
        <SOAP-ENV:Body>
            <ns2:AbilitiesResponse xmlns:ns2="http://example.com/soap-web-service">
            <ns2:abilities>,overgrow,chlorophyll</ns2:abilities>
        </ns2:AbilitiesResponse>
    </SOAP-ENV:Body>
</SOAP-ENV:Envelope>
```

- getBaseExperience- Este metodo retorna el base experience de un pokemon

Request
```xml
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:soap="http://example.com/soap-web-service">
    <soapenv:Header/>
    <soapenv:Body>
        <soap:BaseExperienceRequest>
            <soap:name>bulbasaur</soap:name>
        </soap:BaseExperienceRequest>
    </soapenv:Body>
</soapenv:Envelope>
```

Response
```xml
<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">
    <SOAP-ENV:Header/>
    <SOAP-ENV:Body>
        <ns2:BaseExperienceResponse xmlns:ns2="http://example.com/soap-web-service">
            <ns2:base_experience>64</ns2:base_experience>
        </ns2:BaseExperienceResponse>
    </SOAP-ENV:Body>
</SOAP-ENV:Envelope>
```

- getHeldItems - Este metodo retorna los held items de un pokemon

Request
```xml
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:soap="http://example.com/soap-web-service">
    <soapenv:Header/>
    <soapenv:Body>
        <soap:HeldItemsRequest>
            <soap:name>rattata</soap:name>
        </soap:HeldItemsRequest>
    </soapenv:Body>
</soapenv:Envelope>
```

Response
```xml
<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">
    <SOAP-ENV:Header/>
    <SOAP-ENV:Body>
        <ns2:HeldItemsResponse xmlns:ns2="http://example.com/soap-web-service">
            <ns2:held_items>,chilan-berry</ns2:held_items>
        </ns2:HeldItemsResponse>
    </SOAP-ENV:Body>
</SOAP-ENV:Envelope>
```
- getId - Este metodo retorna el id de un pokemon

Request
```xml
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:soap="http://example.com/soap-web-service">
    <soapenv:Header/>
    <soapenv:Body>
        <soap:IdRequest>
            <soap:name>rattata</soap:name>
        </soap:IdRequest>
    </soapenv:Body>
</soapenv:Envelope>
```

Response
```xml
<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">
    <SOAP-ENV:Header/>
    <SOAP-ENV:Body>
        <ns2:IdResponse xmlns:ns2="http://example.com/soap-web-service">
            <ns2:id>19</ns2:id>
        </ns2:IdResponse>
    </SOAP-ENV:Body>
</SOAP-ENV:Envelope>
```

- getLocation - Este metodo retorna la location_are_encounters de un pokemon

Request
```xml
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:soap="http://example.com/soap-web-service">
    <soapenv:Header/>
    <soapenv:Body>
        <soap:LocationRequest>
            <soap:name>rattata</soap:name>
        </soap:LocationRequest>
    </soapenv:Body>
</soapenv:Envelope>
```

Response
```xml
<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">
    <SOAP-ENV:Header/>
    <SOAP-ENV:Body>
        <ns2:LocationResponse xmlns:ns2="http://example.com/soap-web-service">
            <ns2:location_area_encounters>,sinnoh-route-225-area,sinnoh-sea-route-226-area,johto-route-29-area,johto-route-30-area,johto-route-31-area,sprout-tower-2f,sprout-tower-3f,johto-route-32-area,union-cave-1f,union-cave-b1f,union-cave-b2f,johto-route-33-area,johto-route-34-area,burned-tower-1f,burned-tower-b1f,bell-tower-2f,bell-tower-3f,bell-tower-4f,bell-tower-5f,bell-tower-6f,bell-tower-7f,bell-tower-8f,bell-tower-9f,johto-route-38-area,johto-route-39-area,johto-route-42-area,mt-mortar-1f,mt-mortar-lower-cave,mt-mortar-b1f,johto-route-46-area,bell-tower-10f,unknown-all-rattata-area,kanto-route-1-area,kanto-route-2-south-towards-viridian-city,kanto-route-3-area,kanto-route-4-area,kanto-route-5-area,kanto-route-6-area,kanto-route-7-area,kanto-route-8-area,kanto-route-9-area,kanto-route-10-area,kanto-route-11-area,kanto-route-16-area,kanto-route-17-area,kanto-route-18-area,kanto-sea-route-21-area,kanto-route-22-area,tohjo-falls-area,pokemon-mansion-1f,pokemon-mansion-2f,pokemon-mansion-3f,pokemon-mansion-b1f,castelia-city-area,castelia-sewers-area,castelia-sewers-unknown-area-38,relic-passage-castelia-sewers-entrance,alola-route-1-south</ns2:location_area_encounters>
        </ns2:LocationResponse>
    </SOAP-ENV:Body>
</SOAP-ENV:Envelope>
```

- getName - Este metodo retorna el nombre de un pokemon

Request
```xml
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:soap="http://example.com/soap-web-service">
    <soapenv:Header/>
    <soapenv:Body>
        <soap:NameRequest>
            <soap:name>rattata</soap:name>
        </soap:NameRequest>
    </soapenv:Body>
</soapenv:Envelope>
```

Response
```xml
<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">
    <SOAP-ENV:Header/>
    <SOAP-ENV:Body>
        <ns2:NameResponse xmlns:ns2="http://example.com/soap-web-service">
            <ns2:name>rattata</ns2:name>
        </ns2:NameResponse>
    </SOAP-ENV:Body>
</SOAP-ENV:Envelope>
```


